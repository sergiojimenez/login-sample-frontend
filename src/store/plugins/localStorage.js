var USER = 'login-sample-user'

var initUser = localStorage[USER]

var plugin = store => {
  store.subscribe((mutation) => {
    switch (mutation.type) {
      case 'auth/setUser':
        localStorage[USER] = JSON.stringify(mutation.payload)
        break
      case 'auth/resetUser':
        localStorage.removeItem(USER)
        break
    }
  })
}

export default plugin
export { initUser }
