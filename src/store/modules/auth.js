import Vue from 'vue'
import axios from 'axios'
import { initUser } from '../plugins/localStorage'

const state = {
  user: initUser,
  // With this I can handle the loading status of the user entity from a single place.
  status: {
    loading: false,
    error: false,
    message: ''
  }
}

const mutations = {
  setUser (state, user) {
    state.user = JSON.stringify(user)
  },
  resetUser (state) {
    state.user = ''
  },
  setStatus (state, status) {
    Vue.set(state, 'status', status)
  }
}

const actions = {
  setStatus ({ commit }, status) {
    commit('setStatus', { loading: status.loading, error: status.error, message: status.message })
    return { success: true }
  },

  async login ({ commit, dispatch }, user) {
    // Set endpoint
    const endpoint = process.env.VUE_APP_BACKEND + '/user/login'
    // Set loading status to true
    commit('setStatus', { loading: true, error: false, message: '' })
    // Make query to backend and await response
    const response = await axios.post(endpoint, user)
    if (response.status === 200) {
      // If request successful, save the user data in the browser localstorage
      commit('setUser', response.data)
      // Reset loading status
      commit('setStatus', { loading: false, error: false, message: '' })
      await dispatch('auth/populate', null, { root: true })
      // Return to the frondend saying everything went ok
      return ({ success: true })
    } else {
      // Set loading to false and inform there's an error with the message gotten from the backend
      commit('setStatus', { loading: false, error: true, message: response.message })
      // If there's an error return to the frontend and make that known
      return ({ success: false })
    }
  },

  async register ({ commit }, user) {
    const endpoint = process.env.VUE_APP_BACKEND + '/user/register'
    commit('setStatus', { loading: true, error: false, message: '' })
    const response = await axios.put(endpoint, user)
    if (response.status === 201) {
      commit('setStatus', { loading: false, error: false, message: '' })
      return ({ success: true })
    } else {
      commit('setStatus', { loading: false, error: true, message: response.message })
      return ({ success: false })
    }
  },

  async resetPassword ({ commit }, email) { // Send reset password email
    const endpoint = process.env.VUE_APP_BACKEND + '/user/reset-password'
    commit('setStatus', { loading: true, error: false, message: '' })
    const response = await axios.post(endpoint, email)
    if (response.status === 200) {
      commit('setStatus', { loading: false, error: false, message: 'A link to reset your password was sent to the email you provided.' })
      return { success: true, message: response.message }
    } else {
      commit('setStatus', { loading: false, error: true, message: response.message || 'There was an error with your request, please try again later' })
      return ({ success: false })
    }
  },

  async changePassword ({ commit, getters }, item) { // Change password...
    const endpoint = process.env.VUE_APP_BACKEND + '/user/set-password'
    commit('setStatus', { loading: true, error: false, message: '' })
    try {
      await axios.post(endpoint, { token: item.token, password: item.password })
      commit('setStatus', { loading: false, error: false, message: 'Your password has been reset.' })
      return { success: true, message: 'Your password has been reset.' }
    } catch (error) {
      let message = ''
      if (error.response) {
        message = error.response.data.message
      } else if (error.request) {
        message = 'No response received from the server. If the problem persists contact the site administrator (' + error.message + ')'
      } else {
        message = error.message
      }
      commit('setStatus', { loading: false, error: true, message: message })
      return { success: false }
    }
  },

  async populate ({ commit, dispatch, rootGetters }) {
    // Define endpoint
    const endpoint = process.env.VUE_APP_BACKEND + '/user'
    // Define headers
    const headers = { 'Authorization': 'Bearer ' + rootGetters['auth/getToken'] }
    commit('setStatus', { loading: true, error: false, message: '' })
    const response = await axios.get(endpoint, { headers: headers })
    if (response.status === 200) {
      commit('setUser', response.data)
      // Dispatch action from company module
      // await dispatch('company/populate', rootGetters['auth/currentUser'].organisation_id, { root: true })
      commit('setStatus', { loading: false, error: false, message: '' })
      return ({ success: true })
    } else {
      commit('setStatus', { loading: false, error: true, message: response.message })
      return ({ success: false })
    }
  },

  async edit ({ commit, dispatch, rootGetters }, user) {
    const endpoint = process.env.VUE_APP_BACKEND + '/user'
    const headers = { 'Authorization': 'Bearer ' + rootGetters['auth/getToken'] }
    commit('setStatus', { loading: true, error: false, message: '' })
    const response = await axios.put(endpoint, user, { headers: headers })
    if (response.status === 200 || response.status === 204) {
      await dispatch('auth/populate', null, { root: true })
      commit('setStatus', { loading: false, error: false, message: '' })
      return ({ success: true })
    } else {
      commit('setStatus', { loading: false, error: true, message: response.message })
      return ({ success: false })
    }
  },

  logout ({ commit, dispatch }) {
    commit('resetUser')
    // dispatch('company/resetCompany', null, { root: true })
    commit('setStatus', { loading: false, error: false, message: 'You logged out' })
  }
}

const getPayload = (token) => {
  const payload = token.split('.')[1]
  return JSON.parse(atob(payload))
}

const getters = {
  getToken (state) {
    return state.token ? state.token : false
  },
  isLoggedIn (state) {
    return state.token ? getPayload(state.token).exp > (Date.now() / 1000) : false
  },
  currentUser (state) {
    return state.user ? JSON.parse(state.user) : {}
  },
  isOrganisation (state) {
    return JSON.parse(state.user).type === 'organisation'
  },
  isAdmin (state) {
    return JSON.parse(state.user).type === 'admin'
  },
  getStatus (state) {
    return state.status
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
