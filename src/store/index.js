import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import localStoragePlugin from './plugins/localStorage'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: { auth },
  plugins: [localStoragePlugin]
})
