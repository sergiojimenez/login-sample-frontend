import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store/' // '@' is an alias for 'root/src'
// Check https://vuetifyjs.com/en/framework/a-la-carte to learn about vuetify-loader and why it's cool.
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
// import '@/stylus/main.styl'

Vue.use(Vuetify, {
  // custom theming Vuetify colors
  theme: {
    primary: '#29aafe',
    'green-button': '#87cc61',
    'background-grey': '#e5e7ed',
    'background-dark-grey': '#616161',
    'background-blue': '#29aafe',
    'custom-grey': '#96a2b2'
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
