import Vue from 'vue'
import Router from 'vue-router'
import Store from '@/store'
import axios from 'axios'

Vue.use(Router)

// When building apps with a bundler, the JavaScript bundle can become quite large, and thus affect the page load time.
// It would be more efficient if we can split each route's components into a separate chunk, and only load them when the route is visited.
// The loadView function makes use of Vue's async component feature and webpack's code splitting to achieve this.
// More information: https://router.vuejs.org/guide/advanced/lazy-loading.html

function loadView (folder, view) {
  return () => import(/* webpackChunkName: "[request]" */ `@/views/${folder}/${view}.vue`)
}

const router = new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: loadView('user', 'Login')
    },
    // This is the wildcard route used when the browser is directed to any route not defined above,
    // for now it's just the login one but it should be changed to a custom 404 component. Probably.
    {
      path: '*',
      redirect: '/login'
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  const isLoggedIn = Store.getters['auth/isLoggedIn']
  const currentUser = Store.getters['auth/currentUser']
  // const isAdmin = Store.getters['auth/isAdmin']
  const path = to.path.toLowerCase()

  await Store.dispatch('auth/setStatus', { loading: false, error: false, message: '' })
  
  switch (path) {
    case '/login':
      next(isLoggedIn ? { path: '/user/profile' } : true)
      break
    default:
      next()
  }
})

export default router
